﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.Net.Http.Headers;
using System;

namespace Cohort.Web.Rules {
    /// <summary>Redirects non-www URLs to www</summary>
    public class RedirectWwwRule : IRule {
        public virtual void ApplyRule(RewriteContext context) {
            var req = context.HttpContext.Request;
            //if (req.Host.Host.Equals("localhost", StringComparison.OrdinalIgnoreCase) ||
            //    req.Host.Value.StartsWith("dev.", StringComparison.OrdinalIgnoreCase) ||
            //    req.Host.Value.StartsWith("prod.", StringComparison.OrdinalIgnoreCase)) {

            //    context.Result = RuleResult.ContinueRules;
            //    return;
            //}

            if (req.Host.Value.StartsWith("www.", StringComparison.OrdinalIgnoreCase)) {
                var wwwHost = new HostString(req.Host.Value.Replace("www.", ""));
                var newUrl = UriHelper.BuildAbsolute(req.Scheme, wwwHost, req.PathBase, req.Path, req.QueryString);

                var response = context.HttpContext.Response;
                response.StatusCode = 301;
                response.Headers[HeaderNames.Location] = newUrl;
                context.Result = RuleResult.EndResponse;



                return;
            }

           
        }
    }
}
